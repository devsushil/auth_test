const mongooseConfig = require('./src/config/database');
const app = require('./src/config/express');
const config = require('./src/config/vars');

const connectToDb = () => {
    mongooseConfig
        .connect()
        .then(() => {
            console.log("Database connected successfully")
        }).catch(err => {
            setTimeout(connectToDb, 5000);
        })

}

connectToDb()

app.listen(config.port || 3000, () => {
    console.log("App started")
})