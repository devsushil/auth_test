const app = require("../config/express")
const request = require('supertest')

describe('App integration test', () => {
    it('Should be defined', (done) => {
        expect(app).toBeDefined()
        done()
    })

    let server;

    beforeAll(async () => {
        server = await app.listen(3001)
    })

    afterAll(async () => {
        await server.close()
    })

    describe('Router test', () => {
        it('should be mounted', async () => {
            await request(server).get('/api/status').expect(200)
        })

        it('should be return 404 if route is not found', async () => {
            await request(server).get('/api/404').expect(404)
        })
    })
})