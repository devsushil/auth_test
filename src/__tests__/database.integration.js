const mongooseConfig = require("../config/database")
const mongoose = require('mongoose');

describe('Database connection test', () => {
    afterAll(async () => {
        await mongoose.connection.close();
    })
    it('should sucessfully connect to the test db', async () => {
        await mongooseConfig.connect();
        expect(mongoose.connection.readyState).toBeTruthy()
    })
})