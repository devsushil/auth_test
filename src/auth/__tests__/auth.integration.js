const request = require('supertest')
const faker = require('faker')
const mongoose = require('mongoose');
const app = require('../../config/express');
const mongooseConfig = require('../../config/database');
describe("Auth integration test", () => {
    let server;

    beforeAll(async () => {
        await mongooseConfig.connect()
        server = await app.listen(3001)
    }, 60000)

    afterEach((done) => {
        done();
    });

    afterAll(async () => {
        //Clear the Db
        const collections = mongoose.connection.collections;

        for (const key in collections) {
            const collection = collections[key];
            await collection.deleteMany({});
        }
        await server.close();
        await mongoose.connection.close();
    })

    describe("User registration test", () => {
        it('should send validation error if email is not sent', async () => {
            await request(server).post('/api/auth/register').send({ email: "", password: "Admin123" }).expect(400)
        })

        it('should send validation error if password is not sent', async () => {
            await request(server).post('/api/auth/register').send({ email: faker.internet.email(), password: "" }).expect(400)
        })

        it('should send validation error if email and password is not sent', async () => {
            await request(server).post('/api/auth/register').send({ email: "", password: "" }).expect(400)
        })

        it('should successfully register the user with correct email and password', async () => {
            const response = await request(server).post('/api/auth/register').send({ email: faker.internet.email(), password: faker.internet.password() }).expect(200)
            expect(response.body).toHaveProperty("_id");
            expect(response.body).toHaveProperty("message", "User created successfully");
        })
    })
})