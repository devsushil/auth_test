const AuthService = require("../auth.service")
const sinon = require('sinon');

describe("Auth Service test", () => {
    it('Should be defined', () => {
        expect(AuthService).toBeDefined()
    })

    describe("Register user test", () => {
        const UserServiceMock = {
            saveUser: sinon.spy()
        }

        const AuthDriverMock = {
            hashPassword: sinon.spy()
        }

        it('Should throw validation error if email or password is not passed', async () => {
            try {
                const data = {
                    email: "",
                    password: ""
                }
                const authService = new AuthService(UserServiceMock, AuthDriverMock);
                await authService.register(data);
            } catch (error) {
                expect(error).toBeDefined()
                expect(error.length).toBe(2);
            }
        })

        it('Should hash the password and save the user with hashed Password', async () => {
            try {
                const data = {
                    email: faker.internet.email,
                    password: faker.internet.password
                }
                const authService = new AuthService(UserServiceMock, AuthDriverMock);
                await authService.register(data);
                expect(AuthDriverMock.hashPassword.calledOnce).toBeTruthy()
                expect(UserServiceMock.saveUser.calledOnce).toBeTruthy()
            } catch (error) {

            }
        })
    })
})