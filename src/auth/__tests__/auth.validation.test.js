const faker = require('faker');
const { validateRegister } = require("../auth.validation")
describe("Validation tests", () => {
    describe("validateRegister test", () => {
        it('should return an array of validation error if password is not passed', async () => {
            const data = {
                email: faker.internet.email(),
            };
            let errors = validateRegister(data);
            expect(errors.length).toBe(1);
            expect(errors[0].type).toBe('Validation Error');
            expect(errors[0].field).toBe('password');
        })

        it('should return an array of validation error if email is  empty', async () => {
            const data = {
                email: "",
                password: faker.internet.password(),
            };
            let errors = validateRegister(data);
            expect(errors.length).toBe(1);
            expect(errors[0].type).toBe('Validation Error');
            expect(errors[0].field).toBe('email');

        })

        it('should return an array of validation error if password is  empty', async () => {
            const data = {
                email: faker.internet.email(),
                password: ""
            };
            let errors = validateRegister(data);
            expect(errors.length).toBe(1);
            expect(errors[0].type).toBe('Validation Error');
            expect(errors[0].field).toBe('password');

        })

        it('should return an array of validation error if email and password is  empty', async () => {
            const data = {
                email: "",
                password: ""
            };
            types = ['email', 'password']
            let errors = validateRegister(data);
            expect(errors.length).toBe(2);
            errors.forEach((error, i) => {
                expect(error.type).toBe('Validation Error');
                expect(error.field).toBe(types[i]);
            })
        })

        it('should return an empty array of if email and password is passed', async () => {
            const data = {
                email: faker.internet.email(),
                password: faker.internet.password(),
            };
            let errors = validateRegister(data);
            expect(errors.length).toBe(0);
        })
    })
})