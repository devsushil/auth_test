const AuthService = require("./index");

const AuthController = {
    registerUser: async (req, res) => {
        const { email, password } = req.body;
        try {
            const user = await AuthService.register({ email, password })
            return res.status(200).json({
                message: "User created successfully",
                _id: user._id
            })
        } catch (error) {
            return res.status(400).json({
                error
            })
        }

    }
}

module.exports = AuthController