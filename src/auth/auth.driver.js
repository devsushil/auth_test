const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const config = require("../config/vars")

const hashPassword = async (password) => {
    return await bcrypt.hash(password, saltRounds);
}

const comparePassword = async (password, hash) => {
    //Returns true or false
    return await bcrypt.compare(password, hash)

}

const generateToken = async ({ ...user }) => {
    return new Promise((resolve, reject) => {
        jwt.sign(
            { _id: user._id },
            config.jwt_secret,
            { algorithm: 'RS256', expiresIn: '1h' },
            (err, token) => {
                if (err) {
                    reject(err)
                }
                resolve(token)
            });

    })
}

const decodeToken = async (token) => {
    return new Promise(resolve, reject => {
        jwt.verify(token, config.jwt_secret, function (err, decoded) {
            if (err) {
                reject("Invalid Token error")
            }
            resolve(decoded)
        });
    })
}

module.exports = {
    hashPassword,
    comparePassword,
    generateToken,
    decodeToken
}