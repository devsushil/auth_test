const express = require('express');
const AuthController = require('./auth.controller');
const AuthRouter = express.Router()

AuthRouter.post('/register', AuthController.registerUser);

module.exports = AuthRouter;