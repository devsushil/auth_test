const { validateRegister } = require('./auth.validation')
class AuthService {
    constructor(UserService, AuthDriver) {
        this.UserService = UserService;
        this.AuthDriver = AuthDriver;
    }

    async register({ email, password }) {
        try {
            const errors = validateRegister({ email, password })
            if (errors && errors.length > 0) {
                throw errors;
            }
            //Hash the password
            const hashedPassword = await this.AuthDriver.hashPassword(password)
            const user = await this.UserService.saveUser({ email, password: hashedPassword })
            return user;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = AuthService