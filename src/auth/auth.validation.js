const { ERRORS } = require("./auth.constants")
const validateRegister = ({ email, password }) => {
    let errors = []
    if (!email || email === '') {
        errors = [...errors, {
            type: 'Validation Error',
            field: 'email',
            message: ERRORS.EMAIL_REQUIRED
        }]
    }

    if (!password || password === '') {
        errors = [...errors, {
            type: 'Validation Error',
            field: 'password',
            message: ERRORS.PASSWORD_REQUIRED
        }]
    }

    return errors
}

module.exports = {
    validateRegister
}