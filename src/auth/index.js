const UserService = require('../user')
const AuthDriver = require('./auth.driver')
const AuthService = require('./auth.service')

module.exports = new AuthService(UserService, AuthDriver)
