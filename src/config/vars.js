require('dotenv').config({})

const config = {
    environment: process.env.NODE_ENV,
    port: process.env.PORT,
    mongo_uri: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TEST : process.env.MONGO_URI,
    jwt_secret: process.env.JWT_SECRET
}

module.exports = config