const express = require('express')
const AuthRouter = require('../auth/auth.route')
const router = express.Router()

router.get('/status', (req, res) => {
    return res.send('Route mounted successfully')
})

router.use('/auth', AuthRouter)


module.exports = router
