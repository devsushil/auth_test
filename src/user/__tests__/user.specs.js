const UserService = require('../user.service')
const sinon = require('sinon');

describe('User Service test', () => {
    it('should be defined', () => {
        expect(UserService).toBeDefined()
    })

    describe('Find all test', () => {
        it('Should call find method on the model', async () => {
            const UserModelMock = {
                find: sinon.spy()
            }

            const userService = new UserService(UserModelMock);
            await userService.findAll({ email: "sushil.pokhrel@gmail.com" })
            expect(UserModelMock.find.calledOnce).toBeTruthy()
        })
    })

    describe('Find one test', () => {
        it('Should call findOne method on the model', async () => {
            const UserModelMock = {
                findOne: sinon.spy()
            }

            const userService = new UserService(UserModelMock);
            await userService.findOne({ email: "sushil.pokhrel@gmail.com" })
            expect(UserModelMock.findOne.calledOnce).toBeTruthy()
        })


    })

    describe('Save user test', () => {
        it('Should call create method on the model', async () => {
            const UserModelMock = {
                create: sinon.spy()
            }
            const userService = new UserService(UserModelMock);
            await userService.saveUser({ email: "sushil.pokhrel@gmail.com" })
            expect(UserModelMock.create.calledOnce).toBeTruthy()
        })


    })

})