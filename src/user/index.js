const UserService = require('./user.service')
const UserModel = require('./user.model')

module.exports = new UserService(UserModel)