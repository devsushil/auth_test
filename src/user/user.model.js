const mongoose = require('mongoose')
const { ERRORS } = require('./user.constants')

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: ERRORS.EMAIL_REQUIRED,
        index: true
    },
    password: {
        type: String,
        required: ERRORS.PASSWORD_REQUIRED,
        index: true
    }
})

const User = mongoose.model('User', UserSchema);

module.exports = User;