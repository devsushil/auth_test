class UserService {

    constructor(UserModel) {
        this.UserModel = UserModel
    }

    async findAll({ ...params }) {
        let users = await this.UserModel.find(params)
        return users
    }

    async findOne({ ...params }) {
        let user = await this.UserModel.findOne(params)
        return user;
    }

    async saveUser({ ...params }) {
        let user = await this.UserModel.create(params)

        return user;
    }


}

module.exports = UserService